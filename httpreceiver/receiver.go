package httpreceiver

import (
	"io/ioutil"
	"net/http"
)

type Receiver struct {
	unmarshaller Unmarshaller
}

func (r *Receiver) Receive(request *http.Request) (interface{}, error) {
	if request.ContentLength != 0 {
		data, err := ioutil.ReadAll(request.Body)
		if err != nil {
			return nil, err
		}
		i, err := r.unmarshaller.Unmarshal(data)
		if err != nil {
			return nil, err
		}
		return i, nil
	} else {
		return nil, nil
	}
}

func (r *Receiver) MediaType() string {
	return r.unmarshaller.MediaType()
}

func New(unmarshaller Unmarshaller) *Receiver {
	return &Receiver{
		unmarshaller: unmarshaller,
	}
}

func NewJSON() *Receiver {
	return New(JSONUnmarshaller)
}

