package httpreceiver

import (
	"github.com/pkg/errors"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestReceiver_Receive_IncorrectJSON(t *testing.T) {
	req := httptest.NewRequest("POST", "/", strings.NewReader("incorrect JSON"))
	r := NewJSON()
	i, err := r.Receive(req)
	require.Nil(t, i)
	require.NotNil(t, err)
}

type mockBody struct {
	mock.Mock
}

func (m *mockBody) Read(data []byte) (int, error) {
	args := m.Called(data)
	return args.Int(0), args.Error(1)
}

func TestReceiver_Receive_BadRead(t *testing.T) {
	m := &mockBody{}
	m.On("Read", mock.Anything).Return(0, errors.New("read err"))
	req := httptest.NewRequest("POST", "/", m)
	r := NewJSON()
	i, err := r.Receive(req)
	require.Nil(t, i)
	require.NotNil(t, err)
}

func TestReceiver_Receive_EmptyString(t *testing.T) {
	req := httptest.NewRequest("POST", "/", strings.NewReader(""))
	r := NewJSON()
	i, err := r.Receive(req)
	require.Nil(t, err)
	require.Nil(t, i)
}

func TestReceiver_Receive_SimpleJSON(t *testing.T) {
	req := httptest.NewRequest("POST", "/", strings.NewReader(`{"hello": "world"}`))
	r := NewJSON()
	i, err := r.Receive(req)
	require.Nil(t, err)
	require.NotNil(t, i)
	require.Equal(t, map[string]interface{}{"hello": "world"}, i)
}
