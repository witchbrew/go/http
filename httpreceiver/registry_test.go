package httpreceiver

import (
	"github.com/stretchr/testify/require"
	"net/http/httptest"
	"testing"
)

func TestRegistry(t *testing.T) {
	rec := NewJSON()
	r := NewRegistry(rec)
	req := httptest.NewRequest("GET", "/", nil)
	rec2, err := r.Receiver(req)
	require.Nil(t, err)
	require.Equal(t, rec, rec2)
	req.Header.Add("Content-Type", "invalid content type")
	_, err = r.Receiver(req)
	require.NotNil(t, err)
	require.Equal(t, `cannot read request with content type "invalid content type"`, err.Error())
}

type testUnmarshaller struct{}

func (um *testUnmarshaller) MediaType() string { return "test-content-type" }
func (um *testUnmarshaller) Unmarshal(data []byte) (interface{}, error) {
	return JSONUnmarshaller.Unmarshal(data)
}

func TestRegistry_Register(t *testing.T) {
	r := NewRegistry(NewJSON())
	rec := New(&testUnmarshaller{})
	r.RegisterWithDefault(rec, true)
	req := httptest.NewRequest("GET", "/", nil)
	s2, err := r.Receiver(req)
	require.Nil(t, err)
	require.Equal(t, rec, s2)
	req.Header.Add("Content-Type", rec.MediaType())
	s2, err = r.Receiver(req)
	require.Nil(t, err)
	require.Equal(t, rec, s2)
}
