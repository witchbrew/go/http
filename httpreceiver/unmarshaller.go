package httpreceiver

import "encoding/json"

type Unmarshaller interface {
	MediaType() string
	Unmarshal([]byte) (interface{}, error)
}

type jsonUnmarshaller struct{}

func (um *jsonUnmarshaller) MediaType() string {
	return "application/json"
}

func (um *jsonUnmarshaller) Unmarshal(data []byte) (interface{}, error) {
	var i interface{}
	err := json.Unmarshal(data, &i)
	if err != nil {
		return nil, err
	}
	return i, nil
}

var JSONUnmarshaller = &jsonUnmarshaller{}
