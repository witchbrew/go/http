package httpreceiver

import (
	"github.com/pkg/errors"
	"net/http"
)

type Registry struct {
	defaultReceiver *Receiver
	receivers       map[string]*Receiver
}

func NewRegistry(receiver *Receiver, receivers ...*Receiver) *Registry {
	r := &Registry{
		defaultReceiver: receiver,
		receivers:       map[string]*Receiver{},
	}
	r.Register(receiver)
	for _, receiver := range receivers {
		r.Register(receiver)
	}
	return r
}

func (r *Registry) Register(receiver *Receiver) {
	r.RegisterWithDefault(receiver, false)
}

func (r *Registry) RegisterWithDefault(receiver *Receiver, isDefault bool) {
	r.receivers[receiver.MediaType()] = receiver
	if r.defaultReceiver == nil || isDefault {
		r.defaultReceiver = receiver
	}
}

func (r *Registry) Receiver(request *http.Request) (*Receiver, error) {
	contentTypeHeader := request.Header.Get("Content-Type")
	if contentTypeHeader == "" {
		return r.defaultReceiver, nil
	}
	receiver, ok := r.receivers[contentTypeHeader]
	if !ok {
		return nil, errors.Errorf(`cannot read request with content type "%s"`, contentTypeHeader)
	}
	return receiver, nil
}
