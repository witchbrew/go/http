package httptestclient

import (
	"net/http"
	"strings"
)

type Request struct {
	client *Client
	method string
	url    string
	header http.Header
	body   interface{}
}

func (r *Request) Body(body interface{}) *Request {
	if strings.ToUpper(r.method) == "GET" {
		panic("cannot attach body to GET request")
	}
	r.body = body
	return r
}

func (r *Request) Send() *Response {
	return r.client.send(r)
}
