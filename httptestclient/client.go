package httptestclient

import (
	"github.com/stretchr/testify/require"
	"gitlab.com/witchbrew/go/http/httpreceiver"
	"gitlab.com/witchbrew/go/http/httpsender"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

type Client struct {
	t            *testing.T
	router       http.Handler
	marshaller   httpsender.Marshaller
	unmarshaller httpreceiver.Unmarshaller
}

func New(t *testing.T, router http.Handler, m httpsender.Marshaller, um httpreceiver.Unmarshaller) *Client {
	c := Client{
		t:            t,
		router:       router,
		marshaller:   m,
		unmarshaller: um,
	}
	return &c
}

func NewJSON(t *testing.T, router http.Handler) *Client {
	return New(t, router, httpsender.JSONMarshaller, httpreceiver.JSONUnmarshaller)
}

func (c *Client) newRequest(method string, url string) *Request {
	return &Request{
		client: c,
		method: method,
		url:    url,
		header: http.Header{},
		body:   nil,
	}
}

func (c *Client) send(req *Request) *Response {
	writer := httptest.NewRecorder()
	var reqData []byte
	if req.body != nil {
		data, err := c.marshaller.Marshal(req.body)
		require.Nil(c.t, err)
		reqData = data
	}
	request := httptest.NewRequest(req.method, req.url, strings.NewReader(string(reqData)))
	request.Header.Add("Content-Type", c.marshaller.MediaType())
	c.router.ServeHTTP(writer, request)
	response := writer.Result()
	responseContentType := response.Header.Get("Content-Type")
	require.Equal(c.t, c.unmarshaller.MediaType(), responseContentType)
	var respBody interface{}
	if response.ContentLength != 0 {
		respData, err := ioutil.ReadAll(response.Body)
		require.Nil(c.t, err)
		respBody, err = c.unmarshaller.Unmarshal(respData)
		require.Nil(c.t, err)
	}
	return &Response{
		StatusCode: response.StatusCode,
		Headers:    response.Header,
		Body:       respBody,
	}
}

func (c *Client) Post(url string) *Request {
	return c.newRequest("POST", url)
}

func (c *Client) Get(url string) *Request {
	return c.newRequest("GET", url)
}

func (c *Client) Put(url string) *Request {
	return c.newRequest("GET", url)
}

func (c *Client) Delete(url string) *Request {
	return c.newRequest("DELETE", url)
}
