package httphandler

import (
	"fmt"
	"github.com/rs/zerolog"
	"gitlab.com/witchbrew/go/errorhandling"
	"gitlab.com/witchbrew/go/http/httpsender"
	"net/http"
)

type Response struct {
	requestID  string
	logger     *zerolog.Logger
	writer     http.ResponseWriter
	sender     *httpsender.Sender
	statusCode int
	body       interface{}
	header     http.Header
}

func (r *Response) Header() http.Header {
	return r.header
}

func (r *Response) Empty(statusCode int) {
	r.Body(statusCode, nil)
}

func (r *Response) Body(statusCode int, body interface{}) {
	r.statusCode = statusCode
	r.body = body
}

func (r *Response) logAndWriteErr(e error) {
	r.logger.Error().Err(e).Msg("unhandled error")
	r.writer.WriteHeader(http.StatusInternalServerError)
	r.writer.Header().Add("Content-Type", "text/plain")
	msg := fmt.Sprintf(`Unexpected error, requestID="%s"`, r.requestID)
	bytesWritten, err := r.writer.Write([]byte(msg))
	if err != nil {
		r.logger.Error().Err(e).Msg("could not write unhandled error response")
	}
	if bytesWritten != len(msg) {
		f := "bad write of unhandled error response: actual bytes %d, written bytes %d"
		r.logger.Error().Err(e).Msgf(f, len(msg), bytesWritten)
	}
}

func (r *Response) send() {
	err := r.sender.Send(r.writer, r.statusCode, r.header, r.body)
	if err != nil {
		r.logAndWriteErr(err)
	}
	r.logger.Debug().Int("responseStatusCode", r.statusCode).Msg("sent response")
}

func (r *Response) Error(statusCode int, e *Error) {
	m := map[string]interface{}{
		"type":    e.Type,
		"message": e.Message,
	}
	if e.Values != nil {
		m["values"] = e.Values
	}
	r.Body(statusCode, m)
}

func (r *Response) InformativeError(statusCode int, message string) {
	r.Error(statusCode, &Error{
		Type:    "informative",
		Message: message,
		Values:  nil,
	})
}

func (r *Response) ProcessError(e error) {
	handledErr, ok := e.(*errorhandling.HandledError)
	if ok {
		r.ProcessHandledError(handledErr)
	} else {
		r.logger.Error().Err(e).Msg("unhandled error")
		r.InternalServerError("Unexpected error")
	}
}

func (r *Response) ProcessHandledError(e *errorhandling.HandledError) {
	switch e.Type {
	case errorhandling.BadInput:
		r.BadRequestError(e.Message)
	case errorhandling.NotFound:
		r.NotFoundError(e.Message)
	case errorhandling.Unknown:
		r.InternalServerError(e.Message)
	default:
		r.InternalServerError(e.Message)
	}
}

func (r *Response) InformativeErrorf(statusCode int, format string, args ...interface{}) {
	r.InformativeError(statusCode, fmt.Sprintf(format, args...))
}

func (r *Response) BadRequestError(message string) {
	r.InformativeError(http.StatusBadRequest, message)
}

func (r *Response) BadRequestErrorf(format string, args ...interface{}) {
	r.InformativeErrorf(http.StatusBadRequest, format, args...)
}

func (r *Response) NotFoundError(message string) {
	r.InformativeError(http.StatusNotFound, message)
}

func (r *Response) NotFoundErrorf(format string, args ...interface{}) {
	r.InformativeErrorf(http.StatusNotFound, format, args...)
}

func (r *Response) InternalServerError(message string) {
	r.Error(http.StatusInternalServerError, &Error{
		Type:    "internal-server",
		Message: message,
		Values:  ErrorValues{"requestID": r.requestID},
	})
}

func (r *Response) InternalServerErrorf(format string, args ...interface{}) {
	r.InternalServerError(fmt.Sprintf(format, args...))
}
