package httphandler

type ErrorValues = map[string]interface{}

type Error struct {
	Type    string
	Message string
	Values  ErrorValues
}
