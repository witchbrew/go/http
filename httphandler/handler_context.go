package httphandler

import "github.com/rs/zerolog"

type HandlerContext struct {
	Logger   *zerolog.Logger
	Response *Response
	Request  *Request
}
