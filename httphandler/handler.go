package httphandler

import (
	"github.com/rs/zerolog"
	"gitlab.com/witchbrew/go/chiutils/middleware"
	"gitlab.com/witchbrew/go/http/httpreceiver"
	"gitlab.com/witchbrew/go/http/httpsender"
	"net/http"
)

type HandlerFunc = func(ctx *HandlerContext)

type Registries struct {
	ReceiverRegistry *httpreceiver.Registry
	SenderRegistry   *httpsender.Registry
}

func (r *Registries) WrapHandlerFunc(handlerFunc HandlerFunc) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		requestID := middleware.GetRequestID(request.Context())
		logger := zerolog.Ctx(request.Context())
		logger.
			Debug().
			Str("method", request.Method).
			Str("URI", request.RequestURI).
			Msg("new request")
		handlerContext := &HandlerContext{
			Logger: logger,
			Response: &Response{
				requestID:  requestID,
				logger:     logger,
				writer:     writer,
				sender:     nil,
				statusCode: 0,
				body:       nil,
				header:     nil,
			},
		}
		sender, err := r.SenderRegistry.Sender(request)
		if err != nil {
			handlerContext.Response.logAndWriteErr(err)
			return
		}
		logger.Debug().Str("senderContentType", sender.MediaType()).Msg("sender selected")
		handlerContext.Response.sender = sender
		receiver, err := r.ReceiverRegistry.Receiver(request)
		if err != nil {
			handlerContext.Response.BadRequestError(err.Error())
			handlerContext.Response.send()
			return
		}
		if receiver == nil {
			handlerContext.Response.Empty(http.StatusNotAcceptable)
			handlerContext.Response.send()
			return
		}
		logger.Debug().Str("receiverContentType", receiver.MediaType()).Msg("receiver selected")
		requestBody, err := receiver.Receive(request)
		if err != nil {
			handlerContext.Response.BadRequestError(err.Error())
			handlerContext.Response.send()
			return
		}
		handlerContext.Request = &Request{
			id:          requestID,
			httpRequest: request,
			body:        requestBody,
		}
		handlerFunc(handlerContext)
		handlerContext.Response.send()
	}
}
