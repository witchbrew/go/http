package httphandler

import (
	"github.com/go-chi/chi"
	"net/http"
)

type Request struct {
	id          string
	httpRequest *http.Request
	body        interface{}
}

func (r *Request) ID() string {
	return r.id
}

func (r *Request) URI() string {
	return r.httpRequest.RequestURI
}

func (r *Request) Method() string {
	return r.httpRequest.Method
}

func (r *Request) URLParam(key string) string {
	return chi.URLParam(r.httpRequest, key)
}

func (r *Request) Body() interface{} {
	return r.body
}
