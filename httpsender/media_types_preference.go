package httpsender

import (
	"github.com/pkg/errors"
	"github.com/shopspring/decimal"
	"mime"
	"sort"
	"strings"
)

type sortableMediaType struct {
	mediaType           string
	q                   decimal.Decimal
	wildcardsComparable int8
}

func newSortableMediaType(mediaType string, q decimal.Decimal) (*sortableMediaType, error) {
	parts := strings.Split(mediaType, "/")
	if len(parts) != 2 {
		return nil, errors.Errorf(`bad media type "%s"`, mediaType)
	}
	tp, stp := parts[0], parts[1]
	if tp == "*" && stp != "*" {
		return nil, errors.Errorf(`bad media type "%s"`, mediaType)
	}
	wildcardsComparable := int8(0)
	if tp == "*" {
		wildcardsComparable += 2
	} else if stp == "*" {
		wildcardsComparable += 1
	}
	return &sortableMediaType{
		mediaType:           mediaType,
		q:                   q,
		wildcardsComparable: wildcardsComparable,
	}, nil
}

type sortableMediaTypes struct {
	slice []*sortableMediaType
}

func (s *sortableMediaTypes) Len() int {
	return len(s.slice)
}

func (s *sortableMediaTypes) Less(index1, index2 int) bool {
	s1 := s.slice[index1]
	s2 := s.slice[index2]
	if s1.q.Equal(s2.q) {
		return s1.wildcardsComparable < s2.wildcardsComparable
	} else {
		return s2.q.LessThan(s1.q)
	}
}

func (s *sortableMediaTypes) Swap(index1, index2 int) {
	tmp := s.slice[index1]
	s.slice[index1] = s.slice[index2]
	s.slice[index2] = tmp
}

func prioritizeMediaTypes(acceptHeader string) ([]string, error) {
	parts := strings.Split(acceptHeader, ",")
	sortableMediaTypes := &sortableMediaTypes{slice: make([]*sortableMediaType, len(parts))}
	for index, part := range parts {
		mediaType, params, err := mime.ParseMediaType(part)
		if err != nil {
			return nil, errors.Errorf(`failed to parse media type "%s" at %d`, part, index)
		}
		stringQ, ok := params["q"]
		q, _ := decimal.NewFromString("1.0")
		if ok {
			parsedQ, err := decimal.NewFromString(stringQ)
			if err != nil {
				return nil, errors.Errorf(
					`failed to parse q "%s" for media type "%s" at %d`,
					stringQ,
					part,
					index,
				)
			}
			q = parsedQ
		}
		smt, err := newSortableMediaType(mediaType, q)
		if err != nil {
			return nil, err
		}
		sortableMediaTypes.slice[index] = smt
	}
	sort.Stable(sortableMediaTypes)
	mediaTypes := make([]string, len(parts))
	for index, smt := range sortableMediaTypes.slice {
		mediaTypes[index] = smt.mediaType
	}
	return mediaTypes, nil
}
