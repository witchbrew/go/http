package httpsender

import (
	"github.com/pkg/errors"
	"net/http"
	"strconv"
)

type Sender struct {
	marshaller Marshaller
}

func (s *Sender) MediaType() string {
	return s.marshaller.MediaType()
}

func (s *Sender) Send(writer http.ResponseWriter, statusCode int, header http.Header, body interface{}) error {
	if header != nil {
		for key, values := range header {
			for index, value := range values {
				if index == 0 {
					writer.Header().Set(key, value)
				} else {
					writer.Header().Add(key, value)
				}
			}
		}
	}
	writer.Header().Set("Content-Type", s.marshaller.MediaType())
	contentLength := 0
	var bodyData []byte
	if body != nil {
		d, err := s.marshaller.Marshal(body)
		if err != nil {
			return errors.Wrapf(err, "failed to marshal body")
		}
		bodyData = d
		contentLength = len(bodyData)
	}
	writer.Header().Set("Content-Length", strconv.Itoa(contentLength))
	writer.WriteHeader(statusCode)
	if contentLength != 0 {
		bytesWritten, err := writer.Write(bodyData)
		if err != nil {
			return errors.Wrap(err, "failed to write data")
		}
		if bytesWritten != contentLength {
			return errors.Errorf("bad write: content length %d, written %d", contentLength, bytesWritten)
		}
	}
	return nil
}

func New(marshaller Marshaller) *Sender {
	return &Sender{marshaller: marshaller}
}

func NewJSON() *Sender {
	return New(JSONMarshaller)
}
