package httpsender

import "encoding/json"

type Marshaller interface {
	MediaType() string
	Marshal(interface{}) ([]byte, error)
}

type jsonMarshaller struct{}

func (m *jsonMarshaller) MediaType() string {
	return "application/json"
}

func (m *jsonMarshaller) Marshal(i interface{}) ([]byte, error) {
	return json.Marshal(i)
}

var JSONMarshaller = &jsonMarshaller{}
