package httpsender

import (
	"github.com/stretchr/testify/require"
	"net/http/httptest"
	"testing"
)

func TestSenderRegistry(t *testing.T) {
	s := NewJSON()
	r := NewRegistry(s)
	req := httptest.NewRequest("GET", "/", nil)
	s2, err := r.Sender(req)
	require.Nil(t, err)
	require.Equal(t, s, s2)
	req.Header.Set("Accept", "some/some")
	s, err = r.Sender(req)
	require.Nil(t, err)
	require.Nil(t, s)
}

type testMarshaller struct{}

func (m *testMarshaller) MediaType() string {
	return "test/test"
}

func (m *testMarshaller) Marshal(i interface{}) ([]byte, error) {
	return JSONMarshaller.Marshal(i)
}

func TestSenderRegistry_Register(t *testing.T) {
	r := NewRegistry(NewJSON())
	s := New(&testMarshaller{})
	r.RegisterWithDefaults(s, true, true)
	req := httptest.NewRequest("GET", "/", nil)
	s2, err := r.Sender(req)
	require.Nil(t, err)
	require.Equal(t, s, s2)
	req.Header.Set("Accept", s.MediaType())
	s2, err = r.Sender(req)
	require.Nil(t, err)
	require.Equal(t, s, s2)
}

func TestSenderRegistry_RegisterTwoMediaTypesNegotiation(t *testing.T) {
	r := NewRegistry(NewJSON(), New(&testMarshaller{}))
	req := httptest.NewRequest("GET", "/", nil)
	req.Header.Set("Accept", "application/*")
	s, err := r.Sender(req)
	require.Nil(t, err)
	require.Equal(t, "application/json", s.MediaType())
	req.Header.Set("Accept", "test/test")
	s, err = r.Sender(req)
	require.Nil(t, err)
	require.Equal(t, "test/test", s.MediaType())
	req.Header.Set("Accept", "some/some")
	s, err = r.Sender(req)
	require.Nil(t, err)
	require.Nil(t, s)
	req.Header.Set("Accept", "application/json;q=0.8,test/test")
	s, err = r.Sender(req)
	require.Nil(t, err)
	require.Equal(t, "test/test", s.MediaType())
}
