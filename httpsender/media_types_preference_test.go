package httpsender

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestPrioritizeMediaTypes_SimpleTwoMediaTypes(t *testing.T) {
	mt, err := prioritizeMediaTypes("text/html,application/json")
	require.Nil(t, err)
	require.Equal(t, []string{"text/html", "application/json"}, mt)
}

func TestPrioritizeMediaTypes_TwoMediaTypesQPresent(t *testing.T) {
	mt, err := prioritizeMediaTypes("text/html;q=0.8,application/json")
	require.Nil(t, err)
	require.Equal(t, []string{"application/json", "text/html"}, mt)
}

func TestPrioritizeMediaTypes_TwoMediaTypesSubtypeWildcard(t *testing.T) {
	mt, err := prioritizeMediaTypes("text/*,application/json")
	require.Nil(t, err)
	require.Equal(t, []string{"application/json", "text/*"}, mt)
}

func TestPrioritizeMediaTypes_ThreeMediaTypesWildcards(t *testing.T) {
	mt, err := prioritizeMediaTypes("text/*,*/*,application/json")
	require.Nil(t, err)
	require.Equal(t, []string{"application/json", "text/*", "*/*"}, mt)
}
