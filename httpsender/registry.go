package httpsender

import (
	"fmt"
	"net/http"
	"strings"
)

type Registry struct {
	senders map[string]*Sender
}

func NewRegistry(sender *Sender, senders ...*Sender) *Registry {
	r := &Registry{map[string]*Sender{}}
	r.Register(sender)
	for _, sender := range senders {
		r.Register(sender)
	}
	return r
}

func (r *Registry) Register(sender *Sender) {
	r.RegisterWithDefaults(sender, false, false)
}

func (r *Registry) RegisterWithDefaults(sender *Sender, isDefaultForType bool, isDefault bool) {
	mediaType := sender.MediaType()
	parts := strings.Split(mediaType, "/")
	if len(parts) != 2 {
		panic(fmt.Sprintf(`bad media type "%s" for sender %v`, mediaType, sender))
	}
	t, st := parts[0], parts[1]
	if t == "*" && st != "*" {
		panic(fmt.Sprintf(`bad media type "%s" for sender %v`, mediaType, sender))
	}
	r.senders[mediaType] = sender
	typeMediaType := fmt.Sprintf("%s/*", t)
	_, hasDefaultForType := r.senders[typeMediaType]
	if !hasDefaultForType || isDefaultForType {
		r.senders[typeMediaType] = sender
	}
	_, hasDefault := r.senders["*/*"]
	if !hasDefault || isDefault {
		r.senders["*/*"] = sender
	}
}

func (r *Registry) Sender(request *http.Request) (*Sender, error) {
	acceptHeader := request.Header.Get("Accept")
	if acceptHeader == "" {
		return r.senders["*/*"], nil
	}
	mediaTypes, err := prioritizeMediaTypes(acceptHeader)
	if err != nil {
		return nil, err
	}
	for _, mediaType := range mediaTypes {
		sender, ok := r.senders[mediaType]
		if ok {
			return sender, nil
		}
	}
	return nil, nil
}
