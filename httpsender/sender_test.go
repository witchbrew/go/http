package httpsender

import (
	"github.com/pkg/errors"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestSender_Send_SimpleJSON(t *testing.T) {
	s := NewJSON()
	rec := httptest.NewRecorder()
	err := s.Send(rec, http.StatusOK, nil, map[string]interface{}{"hello": "world"})
	require.Nil(t, err)
	require.Equal(t, "application/json", rec.Header().Get("Content-Type"))
	require.Equal(t, `{"hello":"world"}`, rec.Body.String())
}

type incorrectJSON struct{}

func (i *incorrectJSON) MarshalJSON() ([]byte, error) {
	return nil, errors.New("incorrect JSON")
}

func TestSender_Send_MarshalBodyErr(t *testing.T) {
	s := NewJSON()
	rec := httptest.NewRecorder()
	err := s.Send(rec, http.StatusOK, nil, &incorrectJSON{})
	require.NotNil(t, err)
	require.Contains(t, err.Error(), "failed to marshal body")
}

type mockWriter struct {
	mock.Mock
}

func (m *mockWriter) Header() http.Header {
	args := m.Called()
	return args.Get(0).(http.Header)
}

func (m *mockWriter) WriteHeader(statusCode int) {

}

func (m *mockWriter) Write(data []byte) (int, error) {
	args := m.Called(data)
	return args.Int(0), args.Error(1)
}

func TestSender_Send_WriteErr(t *testing.T) {
	s := NewJSON()
	m := &mockWriter{}
	m.On("WriteHeader", http.StatusOK)
	m.On("Header").Return(http.Header{})
	m.On("Write", []byte(`"hello"`)).Return(len(`"hello"`), errors.New("write err"))
	err := s.Send(m, http.StatusOK, nil, "hello")
	require.NotNil(t, err)
	require.Contains(t, err.Error(), "write err")
}

func TestSender_Send_BadWrite(t *testing.T) {
	s := NewJSON()
	m := &mockWriter{}
	m.On("WriteHeader", http.StatusOK)
	m.On("Header").Return(http.Header{})
	m.On("Write", []byte(`"hello"`)).Return(2, nil)
	err := s.Send(m, http.StatusOK, nil, "hello")
	require.NotNil(t, err)
	require.Contains(t, err.Error(), `bad write: content length 7, written 2`)
}

func TestSender_Send_Headers(t *testing.T) {
	s := NewJSON()
	rec := httptest.NewRecorder()
	headers := http.Header{
		"Some":  {"one", "two"},
		"Other": {"something"},
	}
	err := s.Send(rec, http.StatusOK, headers, nil)
	require.Nil(t, err)
	resp := rec.Result()
	headers.Set("Content-Type", s.MediaType())
	headers.Set("Content-Length", "0")
	require.Equal(t, headers, resp.Header)
}
