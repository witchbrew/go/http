module gitlab.com/witchbrew/go/http

go 1.15

require (
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/pkg/errors v0.9.1
	github.com/rs/zerolog v1.20.0
	github.com/shopspring/decimal v1.2.0
	github.com/stretchr/testify v1.6.1
	gitlab.com/witchbrew/go/chiutils v0.0.0-20201005131212-15e5f93ddd7a
	gitlab.com/witchbrew/go/errorhandling v0.0.0-20201011115201-3517a6876788 // indirect
)
