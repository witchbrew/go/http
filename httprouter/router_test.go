package httprouter

import (
	"github.com/stretchr/testify/require"
	"gitlab.com/witchbrew/go/http/httphandler"
	"gitlab.com/witchbrew/go/http/httptestclient"
	"net/http"
	"testing"
)

func TestRouter_NotFound(t *testing.T) {
	r := NewJSON()
	c := httptestclient.NewJSON(t, r)
	response := c.Get("/some").Send()
	expectedBody := map[string]interface{}{"message": `unknown URI "/some"`, "type": "informative"}
	require.Equal(t, http.StatusNotFound, response.StatusCode)
	require.Equal(t, "application/json", response.Headers.Get("Content-Type"))
	require.Equal(t, expectedBody, response.Body)
}

func TestRouter_MethodNotAllowed(t *testing.T) {
	r := NewJSON()
	r.Post("/some", func(context *httphandler.HandlerContext) {
	})
	c := httptestclient.NewJSON(t, r)
	response := c.Get("/some").Send()
	expectedBody := map[string]interface{}{"message": `method "GET" not allowed`, "type": "informative"}
	require.Equal(t, http.StatusMethodNotAllowed, response.StatusCode)
	require.Equal(t, "application/json", response.Headers.Get("Content-Type"))
	require.Equal(t, expectedBody, response.Body)
}

func TestRouter_Get(t *testing.T) {
	r := NewJSON()
	expectedBody := map[string]interface{}{"hello": "world"}
	r.Get("/some", func(context *httphandler.HandlerContext) {
		context.Response.Body(http.StatusOK, expectedBody)
	})
	c := httptestclient.NewJSON(t, r)
	response := c.Get("/some").Send()
	require.Equal(t, http.StatusOK, response.StatusCode)
	require.Equal(t, "application/json", response.Headers.Get("Content-Type"))
	require.Equal(t, expectedBody, response.Body)
}

func TestRouter_Delete(t *testing.T) {
	r := NewJSON()
	r.Delete("/some", func(context *httphandler.HandlerContext) {
		context.Response.Empty(http.StatusOK)
	})
	c := httptestclient.NewJSON(t, r)
	response := c.Delete("/some").Send()
	require.Equal(t, http.StatusOK, response.StatusCode)
	require.Equal(t, "application/json", response.Headers.Get("Content-Type"))
	require.Nil(t, response.Body)
}
