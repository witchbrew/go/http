package httprouter

import (
	"github.com/go-chi/chi"
	"gitlab.com/witchbrew/go/chiutils/middleware"
	"gitlab.com/witchbrew/go/http/httphandler"
	"gitlab.com/witchbrew/go/http/httpreceiver"
	"gitlab.com/witchbrew/go/http/httpsender"
	"net/http"
)

type Router struct {
	chiRouter  chi.Router
	registries *httphandler.Registries
}

func (r *Router) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	r.chiRouter.ServeHTTP(writer, request)
}

func (r *Router) Get(pattern string, handlerFunc httphandler.HandlerFunc) {
	r.chiRouter.Get(pattern, r.registries.WrapHandlerFunc(handlerFunc))
}

func (r *Router) Post(pattern string, handlerFunc httphandler.HandlerFunc) {
	r.chiRouter.Post(pattern, r.registries.WrapHandlerFunc(handlerFunc))
}

func (r *Router) Put(pattern string, handlerFunc httphandler.HandlerFunc) {
	r.chiRouter.Put(pattern, r.registries.WrapHandlerFunc(handlerFunc))
}

func (r *Router) Delete(pattern string, handlerFunc httphandler.HandlerFunc) {
	r.chiRouter.Delete(pattern, r.registries.WrapHandlerFunc(handlerFunc))
}

func New(registries *httphandler.Registries) *Router {
	router := &Router{
		chiRouter:  chi.NewRouter(),
		registries: registries,
	}
	router.chiRouter.Use(middleware.RequestID)
	router.chiRouter.Use(middleware.Zerolog)
	router.chiRouter.Use(middleware.ZerologRequestID)
	router.chiRouter.NotFound(router.registries.WrapHandlerFunc(func(context *httphandler.HandlerContext) {
		context.Response.NotFoundErrorf(`unknown URI "%s"`, context.Request.URI())
	}))
	router.chiRouter.MethodNotAllowed(router.registries.WrapHandlerFunc(func(context *httphandler.HandlerContext) {
		context.Response.InformativeErrorf(
			http.StatusMethodNotAllowed,
			`method "%s" not allowed`,
			context.Request.Method(),
		)
	}))
	return router
}

func NewJSON() *Router {
	receiverRegistry := httpreceiver.NewRegistry(httpreceiver.NewJSON())
	senderRegistry := httpsender.NewRegistry(httpsender.NewJSON())
	return New(&httphandler.Registries{
		ReceiverRegistry: receiverRegistry,
		SenderRegistry:   senderRegistry,
	})
}
